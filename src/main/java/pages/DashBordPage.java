package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import utils.Browser;

public class DashBordPage {

    private static final By LOC_CATALOG = By.className("parent");
    private static final By LOC_CATEGORIES = By.linkText("Categories");
    private static final By LOC_PRODUCTS = By.linkText("Products");
    private static final By LOC_FILTER = By.linkText("Filters");
    private static final By LOC_OPTIONS= By.linkText("Options");
    private static final By LOC_MANUFACTURERS = By.linkText("Manufacturers");
    private static final By LOC_REVIEWS = By.linkText("Reviews");
    private static final By LOC_INFORMATION = By.linkText("Information");
    private static final By LOC_DOWNLOADS = By.linkText("Downloads");
    private static final By LOC_ATTRIBUTES = By.linkText("Attributes");


//test



    /**
     * This method clicks on catalog
     */
    public static void clickCatalog() {
        Browser.driver.findElement(LOC_CATALOG).click();
    }


    /**
     * This method clicks on Categories
     */
   public static void clickCategories () {
       Browser.driver.findElement(LOC_CATEGORIES).click();
   }

    /**
     * Method for checking clickCategories
     */

    public static void categoriesCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Categories");
    }


    /**
     * This method clicks on Products
     */
   public static void clickProducts () {
       Browser.driver.findElement (LOC_PRODUCTS).click();
   }

    /**
     * Method for checking clickProducts
     */

    public static void productsCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Products");
    }

    /**
     * This method clicks on filter
     */

   public static void clickFilter () {
       Browser.driver.findElement(LOC_FILTER).click();
   }

    /**
     * Method for checking clickFilter
     */

    public static void filterCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Filters");
    }

    /**
     * This method clicks on options
     */

    public static void clickOptions () {
        Browser.driver.findElement(LOC_OPTIONS).click();
    }

    /**
     * Method for checking clickOptions
     */

    public static void optionsCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Options");
    }

    /**
     * This method clicks on manufacturers
     */

    public static void clickManufacturers () {
        Browser.driver.findElement(LOC_MANUFACTURERS).click();
    }

    /**
     * Method for checking clickManufacturers
     */

    public static void manufacturersCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Manufacturers");
    }

    /**
     * This method clicks on reviews
     */

    public static void clickReviews () {
        Browser.driver.findElement(LOC_REVIEWS).click();
    }

    /**
     * Method for checking clickReviews
     */

    public static void reviewsCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Reviews");
    }

    /**
     * This method clicks on information
     */

    public static void clickInformation () {
        Browser.driver.findElement(LOC_INFORMATION).click();
    }

    /**
     * Method for checking clickInformation
     */

    public static void informationCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Information");
    }

    /**
     * This method clicks on attributes
     */

    public static void clickAttributes () {
       Browser.driver.findElement(LOC_ATTRIBUTES).click();
    }

    /**
     * Method for checking clickAttributes
     */

    public static void attributesCheck () {
        WebElement attributest =  Browser.driver.findElement(By.linkText("Attributes"));
        attributest.click();
        Select drop = new Select(attributest);
        Assert.assertFalse(drop.isMultiple());
        Assert.assertEquals(drop.getOptions().size(),"2");
    }

    /**
     * This method clicks on downloads
     */

    public static void clickDownloads () {
        Browser.driver.findElement(LOC_DOWNLOADS).click();
    }

    /**
     * Method for checking clickDownloads
     */

    public static void downloadsCheck () {
        WebElement catalog = Browser.driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div/h1"));
        Assert.assertEquals(catalog.getText(), "Downloads");
    }






}
