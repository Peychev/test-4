package admin.positive;

import com.sun.deploy.cache.SignedAsBlobJarFile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AdminLoginPage;
import pages.DashBordPage;
import utils.Browser;

import java.util.concurrent.TimeUnit;

public class DashBoardPageTest {



    @BeforeMethod
    public void setUP() {
        Browser.open("chrome");
    }

    @Test
    public void dashboardClickTest() {
        AdminLoginPage.goTo();
        AdminLoginPage.login("admin", "parola123!");
        DashBordPage.clickCatalog();
        DashBordPage.clickCategories();
        DashBordPage.categoriesCheck();
        DashBordPage.clickFilter();
        DashBordPage.filterCheck();
        DashBordPage.clickProducts();
        DashBordPage.productsCheck();
        DashBordPage.clickAttributes();
        //DashBordPage.clickAttributesPrime();
        DashBordPage.attributesCheck();
        DashBordPage.clickDownloads();
        DashBordPage.downloadsCheck();
        DashBordPage.clickManufacturers();
        DashBordPage.manufacturersCheck();
        DashBordPage.clickInformation();
        DashBordPage.informationCheck();
        DashBordPage.clickOptions();
        DashBordPage.optionsCheck();
        DashBordPage.clickReviews();
        DashBordPage.reviewsCheck();
    }

   // @AfterMethod
    public void tearDown () {
        Browser.quit();
    }
}

